Requirements for Portfolio webpage for Dr. Diahann A.M. Atacho. 

Webpage should be divided into few sections 
    - Front page
    - Biography
    - Projects
    - Publication
    - Contacts

The webpage needs to be responsive. In desktop mode each section should cover the whole screen. For smaller screens the sections should be divided into articles that cover the whole whole screens.  

Front page
should contain image of Diahann, A navbar to navigate to each section and a small introductury text about Diahann.

Navbar
The idea is that the Navbar is visible on frontpage but is hidden in other sections unless hovered. subject to change if this is bad ux.

Biography
Long text about history of Diahann.

Projects
Each project gets a card component that if clicked will open a modal about the project

Publications
List of Publications

Contacts
Icons with social medias and other means to contact Diahann  
    
scrolling down the page should automatically scroll you between each section with one scroll in smaller screens you scroll between articles in each section.
