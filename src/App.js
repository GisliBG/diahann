import React, { useEffect } from 'react'
import useWindowSize from './hooks'
import Header from './components/Header'
import Navigation from './components/Navigation'
import Biography from './components/Biography'
import Main from './components/Main'
import Section from './components/Section'
import Frontpage from './components/Frontpage'
import Projects from './components/Projects'

import './App.css';

function App() {
  const windowSize = useWindowSize()
  useEffect(() => {
    console.log(windowSize)
  }, [windowSize])

  return (
    <React.Fragment>
      {windowSize.breakpoint('xl') ? (
      <Header>
        <Navigation/>
      </Header>
      ) : null}
    <Main>
      {windowSize.breakpoint('xl') ? (
        <React.Fragment>
          <Section id="#" color="red">
            <Frontpage/>
            <Biography/>
          </Section>
          <Section id="projects" color="green">
            <Projects />
          </Section>
        </React.Fragment>
      ) : (
        <React.Fragment>
          <Section id="#" color="red">
            <Frontpage />
          </Section>
          <Section id="bio" color="blue">
            <Biography  />
          </Section>
          <Section id="projects" color="green">
            <Projects />
          </Section>
        </React.Fragment>
      )}
      
      {/*
      <Section>
        <Contact/>
      </Section> */}
      </Main>
    </React.Fragment>
  );
}

export default App;
