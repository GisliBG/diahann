import React from 'react'
import classNames from 'classnames'
import styles from './NavButton.module.scss'

export const NavButton = ({className}) => {
    
    return (
        <nav className={classNames(styles.wrapper, className)}>
            {/* <h2 className={styles.tooltip}>Look!</h2> */}
            <div href="#" className={styles.pluss}>
                <ul>
                    <li><a href="#bio">Biography</a></li>
                    <li><a href="#projects">Projects</a></li>
                    <li><a href="#contacts">Contact</a></li>
                </ul>
            </div>
        </nav> 
    )
}
