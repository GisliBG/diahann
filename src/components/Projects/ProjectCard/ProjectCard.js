import React, {useCallback, useState} from 'react'
import Card from 'react-bootstrap/Card'
import { ProjectModal } from '../ProjectModal/ProjectModal';

export const ProjectCard = ({color='primary', header, title, text, className}) => {
    const [openModal, setModal] = useState(false);
    const closeModal = useCallback(() => {
        setModal(false)
    }, [setModal])
    return (
        <React.Fragment>
            <ProjectModal open={openModal} onClose={closeModal} />
            <Card 
                bg={color}
                onClick={() => setModal(true)}
                className={className}
            >
                {header ? <Card.Header>{header}</Card.Header> : null }
                <Card.Body>
                    <Card.Title>{title}</Card.Title>
                    <Card.Text>
                        {text}
                    </Card.Text>
                </Card.Body>
            </Card>
        </React.Fragment>
    )
}
