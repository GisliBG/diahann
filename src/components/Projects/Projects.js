import Container from 'react-bootstrap/Container' 
import ProjectCard from './ProjectCard'
import styles from './Projects.module.css'

const projectCards = [
    {id: 1, title: "Esse esse in cillum", text: "Veniam magna aliqua aliquip amet exercitation."},
    {id: 2, title: "Voluptate excepteur fugiat", text: "Lorem occaecat in aute amet sint cillum duis."},
    {id: 3, title: "Ad aliquip", text: "Adipisicing consequat aliquip labore laboris voluptate esse consectetur irure veniam ullamco aute."}
]

export const Projects = () => {
    return (
        <Container fluid className={styles.parent}>
            {projectCards.map(project => ( 
                <ProjectCard
                    key={project.id} 
                    title={project.title}
                    text={project.text}
                    className={styles.item}
                />)
            )}
        </Container>
    )
}
