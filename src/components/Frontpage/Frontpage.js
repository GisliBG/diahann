import React from 'react'
import styles from './Frontpage.module.css';
import Container from 'react-bootstrap/Container'
import NavButton from '../NavButton'

export const Frontpage = () => {
    return (
        <Container fluid className={styles.parent}>
            <h1 className={styles.item}>What makes us human ?</h1>
            <div className={styles.item}>Culpa non sunt ea et.Ut ipsum laboris laboris excepteur magna duis est. Officia dolore anim qui ut mollit duis Lorem eu in aliquip labore non proident id.</div>
            <NavButton className={styles.item}/>
        </Container>
    )
}