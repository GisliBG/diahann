import styles from './Biography.module.css'
import Container from 'react-bootstrap/Container'

export const Biography = () => (
    <article>
        <Container fluid className={styles.parent}>
            <p className={styles.item}>Officia cupidatat magna enim in enim sunt do mollit minim voluptate. Laborum mollit occaecat incididunt quis duis nisi velit labore qui fugiat fugiat in dolore. Duis laborum incididunt cupidatat ea eiusmod duis anim id nulla. Aute ipsum aliqua reprehenderit officia. Ipsum sunt culpa irure anim dolore tempor esse. Sunt elit occaecat dolor laborum laboris esse esse.</p>
            <p className={styles.item}>Consectetur ad laboris ipsum cillum. Irure ut culpa amet sunt ad adipisicing id culpa sunt deserunt ullamco proident. Velit culpa ex ex commodo Lorem eiusmod aliqua esse.</p>
            <p className={styles.item}>Eu ex aliqua cillum do. Duis exercitation et id ad excepteur excepteur minim labore cupidatat. Et sint consequat eu in ex adipisicing. Excepteur laborum officia consequat eiusmod excepteur excepteur laborum nostrud labore veniam ad nisi ullamco eu. Laboris ea cupidatat labore qui commodo dolore ut anim esse veniam est fugiat.</p>
        </Container>
    </article>
)