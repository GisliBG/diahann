import React from 'react'
import classNames from 'classnames'
import {useScrolling} from '../../hooks/useScrolls'
import Container from 'react-bootstrap/Container'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import styles from './Navigation.module.css'

export const Navigation = () => {
    const show = useScrolling()
    return (
        <Navbar className={classNames(styles.navigation, 'py-3', show ? styles.active : styles.hidden)} fixed='top'>
            <Navbar.Brand href="#">React-Bootstrap</Navbar.Brand>
            <Container>
                <Nav className="ml-auto">
                    <Nav.Item>
                        <Nav.Link href="#projects">Projects</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link href="#contatcs">Contacts</Nav.Link>
                    </Nav.Item>
                </Nav>
            </Container>
        </Navbar>
    )
}
