import { useState, useCallback, useEffect } from 'react'

export const defaultSize = {
    xs: 0, 
    sm: 576, 
    md: 768, 
    lg: 992, 
    xl: 1200, 
    xxl: 1400
}

export const useWindowSize = (xs = defaultSize.xs, sm = defaultSize.sm, md = defaultSize.md, lg = defaultSize.lg, xl = defaultSize.xl, xxl = defaultSize.xl) => {
    const [windowSize, setWindowSize] = useState({ 
        width: window.innerWidth, 
        height: window.innerHeight,
        xs,
        sm,
        md,
        lg,
        xl,
        xxl
    })

    const handleResize = useCallback(() => {
        setWindowSize((prev) => ({...prev, width: window.innerWidth, height: window.innerHeight}));
    }, [setWindowSize])

    useEffect(() => {
        window.addEventListener('resize', handleResize);

        return () => window.removeEventListener('resize', handleResize);
    }, [ handleResize ])

    
    const isSize = (width) => {
        return windowSize[width] <= windowSize.width
    }
    
    return { breakpoint: isSize, width: windowSize.width, height: windowSize.height};
}