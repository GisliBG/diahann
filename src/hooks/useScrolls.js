import {useCallback, useEffect, useState} from 'react'

export const useScrolling = () => {
    const [show, setShow] = useState(true)
    
    const handleScroll = useCallback((event) => {
        setTimeout(() => {
            setShow(true)
        }, 500)
        setShow(false)
    }, [ setShow])

    useEffect(() => {
        window.addEventListener("scroll", handleScroll);
        return () => {
            window.removeEventListener("scroll", handleScroll)
        }
    }, [handleScroll])

    return show
}